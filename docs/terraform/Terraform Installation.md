## Write step for installing terraform



```
# Download Terraform
wget https://releases.hashicorp.com/terraform/0.12.24/terraform_0.12.24_linux_amd64.zip

# Unzip the downloaded file
unzip terraform_0.12.24_linux_amd64.zip

# Move the executable into a directory included in your system's `PATH`
mv terraform /usr/local/bin/

# Verify the installation
terraform --version
```
