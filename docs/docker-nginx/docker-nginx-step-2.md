# Step 2: Serving a Static HTML Page with Docker and Nginx


This step involves creating a basic `index.html` file, preparing a Dockerfile for it, and running the setup using Docker. This demonstrates how to containerize a static website with Nginx.


## Architecture Diagram

![alt text](docker-nginx.drawio.png)

## Project Structure

Your project directory should look like this:

your-project
-  Dockerfile
-  index.html


- `Dockerfile`: Docker configuration file.
- `index.html`: Your static HTML page.

## Setup Instructions

## Creating the HTML File

First, create a simple `index.html` file. Here's an example:

```html
<!DOCTYPE html>
<html>
<head>
    <title>My Docker-Nginx Page</title>
</head>
<body>
    <h1>Hello from Docker!</h1>
    <p>This is a static HTML page being served by an Nginx container.</p>
</body>
</html>
```

## Creating the Dockerfile

 In your project's root directory, create a `Dockerfile` with the following content:

  ```sh
# Dockerfile
# Use the Nginx image from Docker Hub
FROM nginx:latest
# Copy the index.html file into the container
COPY index.html /usr/share/nginx/html/index.html

```

2. **Building the Docker Image**
   
   In your terminal, navigate to your project directory and run the following command to build your Docker image:

    ```sh
    docker build -t my-nginx-image .
    
    ``` 

3. **Running the Docker Container**:

   After the image has been successfully built, start your container with:

    ```sh
    docker run --name my-nginx-container -d -p 8080:80 my-nginx-image
    ```

   This command will start the Nginx server inside the container and map port 8080 of your host to port 80 of the container.

4. **Accessing the Web Page**:

   Open your browser and visit `http://localhost:8080`. You should see your `index.html` page served by Nginx.

## Common Troubleshooting

- **404 Not Found Error**: Ensure that `index.html` is in the same directory as your `Dockerfile` and that the file name is correct.
- **Changes Not Reflecting**: Remember to rebuild your Docker image and restart the container after making changes to `index.html`.
- **Docker Commands Not Working**: Make sure Docker is running on your system. 

For more detailed information, refer to Docker's official documentation.

---
