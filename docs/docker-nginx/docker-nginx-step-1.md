# Step 1: Introduction to Docker

## What is Docker?

Imagine you're going on a grocery run to pick up five different items: sugar, pickles, flour, eggs, and vegetables. You carry one bag for convenience, but each item is packaged separately. This separation ensures that the items don't get mixed up – your flour stays dry, the eggs remain unbroken, and the vegetables stay fresh. Each item has its own distinct environment within the same bag.

This scenario mirrors the principle of containerization in software deployment. If you have a server where you need to deploy five different applications, you wouldn't just lump them all together. Instead, like your grocery items, each application is placed into its own container. These containers share the same server (like the bag), but they operate in isolated environments. This isolation prevents the applications from interfering with each other, maintaining the integrity and efficiency of your server's resources.

Docker is an open-source platform that automates the deployment of applications inside lightweight, portable containers. These containers can run virtually anywhere, ensuring consistency across multiple development, staging, and production environments.

## Docker vs. Virtual Machines (VMs)

- **Isolation**: Both Docker and VMs provide isolation. VMs isolate at the hardware level via hypervisors, while Docker containers isolate at the application level using the host OS's kernel.
- **Performance**: Containers are generally more lightweight and have less overhead than VMs, leading to better performance and quicker start times.
- **Portability**: Docker containers are highly portable. The same container can run unchanged on a developer's laptop, a test environment, and in production.
- **Resource Efficiency**: Containers share the host system's kernel, so they don't need to boot an OS, consume less memory, and use a fraction of the disk space compared to VMs.

## Why Use Docker?

- **Consistent Environments**: Containers ensure that applications run the same in different environments.
- **Rapid Deployment**: Containers can be started quickly and are suitable for continuous integration/deployment scenarios.
- **Scalability and Modularity**: Easy to scale and distribute containerized applications across various host machines.

## Basic Docker Commands

- **docker run**: Run a container.

    ```
    docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
    ```

- **docker ps**: List running containers.

    ```
    docker ps [OPTIONS]
    ```

- **docker start**: Start one or more stopped containers.

    ```
    docker start [OPTIONS] CONTAINER [CONTAINER...]
    ```

- **docker exec**: Run a command in a running container.

    ```
    docker exec -it CONTAINER COMMAND
    ```

- **docker logs**: Fetch the logs of a container.

    ```
    docker logs [OPTIONS] CONTAINER
    ```

- **docker port**: List port mappings or a specific mapping for the container.

    ```
    docker port CONTAINER [PRIVATE_PORT[/PROTO]]
    ```

- **docker network**: Manage networks.

    ```
    docker network COMMAND
    ```

- **docker attach**: Attach local standard input, output, and error streams to a running container.

    ```
    docker attach [OPTIONS] CONTAINER
    ```

# Let's jump to the practice

As we now have a foundational understanding of Docker, let's dive right into practical applications. In this lab, we'll explore seven distinct phases of Docker utilization:

- Our journey begins with the creation of a Docker container running Nginx, confirming our ability to successfully operate Nginx within Docker.
- Subsequently, we will craft a basic `index.html` file and deploy it using the Nginx server in Docker.
- Moving forward, our focus shifts to developing a two-tier REST API architecture. The backend will be powered by the Express framework in NodeJS, and we will deploy this NodeJS application using a Node-based Docker image.
- Our next endeavor involves building a React JS frontend application, aiming to establish a connection with the NodeJS backend running in a Docker container.
- Following that, we will delve into multiplatform deployment concepts using Docker. This involves building the React JS application in a Node image, but serving it using an Nginx image.
- Penultimately, we'll employ Docker Compose to orchestrate both frontend and backend containers, examining their intercommunication.
- In the final phase, we focus on operational skills. We'll learn how to inspect logs in Docker and navigate within Docker containers for debugging and operational insight.
Each of these steps is designed to progressively build your skills and understanding of Docker's capabilities in different scenarios.

## Running an Nginx Image in Detached Mode

1. **Start Nginx Container**: Run an Nginx container in detached mode (background).
   
    ```
    docker run --name my-first-nginx -d -p 8080:80 nginx
    ```

2. **Check the conatiner is running**: 
2. **Exec into Container**: Access the shell inside the Nginx container.

    ```
    docker exec -it my-nginx /bin/bash
    ```

This concludes Step 1 of the project, where you've learned the basics of Docker, differences between Docker and VMs, reasons to use Docker, and some essential Docker commands. In the next step, we'll delve into serving an HTML file using Nginx in Docker.
