```
mkdir my-express-app
cd my-express-app
npm init -y
npm install express
```

## index.js 

```
const express = require('express')

const app = express()

const port = 8991

app.get('/', (req, res) => {
    res.send("Hello from API")
})

app.get('/orders',(req,res) => {
    res.send("All orders here")
})

app.listen(port, () => {
    console.log("App running on port "+port)
})
```

```
FROM node:alpine
WORKDIR /usr/src/app
COPY index.js .
COPY package*.json .
RUN npm install
EXPOSE 8991
CMD ["node","index.js"]
```


